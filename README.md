
## BRANCHET Thibaud
## GRINE Sameh
## REMANDE Florian

# Tableau des exécutions en ms
## 30 exécutions en local

Votants 100 / Messages 10 = 1 542,8 ms

Votants 100 / Messages 50 = 13 149 ms

Votants 100 / Messages 100 = 141 119 ms


Votants 1000 / Messages 10 = 
51 265 ms

Votants 1000 / Messages 50 = 
257 232 ms

Votants 1000 / Messages 100 = 398 485 ms


Votants 5000 / Messages 10 = 441 934 ms

Votants 5000 / Messages 50 = 1 335 730 ms

Votants 5000 / Messages 100 = > 3 600 000 ms


# Application
![Home]
(https://image.noelshack.com/fichiers/2018/26/5/1530285928-capture-du-2018-06-29-17-25-09.png)


# Development

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

Run `npm install` then `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
