import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angular5-social-login';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { getLocaleDateFormat } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  imageURL: string;
  email: string;
  connected = false;
  name: string;
  token: string;
  private apiUrl = 'https://canard-194515.appspot.com/_ah/api/';
  private posts;
  private postsCopy;
  closeResult: string;
  missingParameter = false;
  private tempsExecution = 0;
  time = 0;

  constructor(
    private http: HttpClient,
    private socialAuthService: AuthService,
    private modalService: NgbModal) { }

  ngOnInit() {
    // get user infos
    this.name = sessionStorage.getItem('user');
    this.imageURL = sessionStorage.getItem('imageUrl');
    if (this.name && this.imageURL) {
      this.connected = true;
    } else {
      this.connected = false;
    }

    // reset timer
    this.resetTimer();
    this.startTimer();
    // récupérer les posts disponibles
    this.http.get(this.apiUrl + 'messages/karmaOrdered?limit=10')
    .subscribe(data => {
      this.stopTimer();
      this.posts = data['items'];
    });

  }

  creerTopicModal(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  creerTopic(contenu) {
    if (contenu !== '') {
      const date = '2018-01-01';
      const author = sessionStorage.getItem('user');
      const post = {
       'body': {'value': contenu},
       'creationDate': date,
       'score' : 0,
      };
      // reset timer
      this.resetTimer();
      this.startTimer();
      this.http.post(this.apiUrl + author + '/messages', post)
      .subscribe(() => {
        this.stopTimer();
        // reload page
        alert('Topic créé avec succès');
        this.http.get(this.apiUrl + 'messages/karmaOrdered?limit=10')
        .subscribe(data => {
          this.posts = data['items'];
        });

      });
    } else {
      alert('Paramètres manquants.');
    }
  }

  public reload() {
    window.location.reload();
  }

  public getRandomId(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    // reset timer
    this.resetTimer();
    this.startTimer();
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        alert('Connexion réussie');
        this.name = userData.name;
        this.imageURL = userData.image;
        sessionStorage.setItem('user', userData.name);
        sessionStorage.setItem('imageUrl', userData.image);
        this.connected = true;
      }
    );
  }

  public logout() {
    sessionStorage.removeItem('user');
    sessionStorage.removeItem('imageUrl');
    alert('Déconnexion réussie');
    this.connected = false;
  }

  public byPopularity() {
    this.postsCopy = this.posts;
    // reset timer
    this.resetTimer();
    this.startTimer();
    this.posts = this.posts.sort(function (post1, post2) {
      this.stopTimer();
      return post1.nbUp > post2.nbUp;
    });
  }

  public myTopics() {
    const username = sessionStorage.getItem('user');
    // reset timer
    this.resetTimer();
    this.startTimer();
    this.http.get(this.apiUrl + username + '/messages?limit=10')
        .subscribe(data => {
          this.stopTimer();
          this.posts = data['items'];
        });
  }

  public topicsVotedByMe() {
    // reset timer
    this.resetTimer();
    // récupérer les posts disponibles
    // getUser
    const currentUser = sessionStorage.getItem('user');
    this.startTimer();
    return this.http.get(this.apiUrl + currentUser + '/messages/voted?limit=10')
    .subscribe(data => {
      this.stopTimer();
      this.posts = data['items'];
    });
  }

  vote(post, choice) {
    // reset timer
    this.resetTimer();
    if (!this.connected) {
      alert('Vous devez être connecté pour voter.');
    } else {
      const author = sessionStorage.getItem('user');
      this.startTimer();
      this.http.get(this.apiUrl + author + '/messages/' + post.id + '?vote=' + choice)
        .subscribe(
          result => {
            this.stopTimer();
            // reload page
            alert('Vote pris en compte.');
            this.http.get(this.apiUrl + 'messages/karmaOrdered?limit=10')
            .subscribe(data => {
              this.posts = data['items'];
            });
          },
          error => {alert(error.error.error.message); },
      );
    }
  }

  public startTimer() {
    this.time = Date.now();
  }

  public stopTimer() {
    this.tempsExecution = Date.now() - this.time;
  }

  public resetTimer() {
    this.time = 0;
    this.tempsExecution = 0;
  }

}
